window.megaSlitherStats = {
    gameDuration: 0,
    timeSpendinGame: 0,
    startTime: 0,
    userStats: [],
    snakPathLength: 0,
    snakePosition: {x:0, y:0},
    original_ws_onmessage: null,
    killsInGame: 0,
    event: [],
    gameId: 0,
    overlays: {},
    sessionRequestSent: false,
    snakeLegth: 0,

    initOverlay: function () {
        var statsOverlay = document.createElement('div');
        statsOverlay.style.position = 'fixed';
        statsOverlay.style.left = '10px';
        statsOverlay.style.bottom = '110px';
        statsOverlay.style.width = '240px';
        // statsOverlay.style.background = 'rgba(0, 0, 0, 0.5)';
        statsOverlay.style.color = '#C0C0C0';
        statsOverlay.style.fontFamily = 'Consolas, Verdana';
        statsOverlay.style.zIndex = 998;
        statsOverlay.style.fontSize = '14px';
        statsOverlay.style.padding = '5px';
        statsOverlay.style.borderRadius = '5px';
        statsOverlay.className = 'nsi';
        document.body.appendChild(statsOverlay);

        megaSlitherStats.overlays.statsOverlay = statsOverlay;

    },
    lengthFormatter: function(value) {
      if(value < 10000){
        return value;
      }
      // thousands
      else if(value >= 10000 && value <= 999999){
        return (value / 1000).toFixed(1) + 'K';
      }
      // millions
      else if(value >= 1000000 && value <= 999999999){
        return (value / 1000000).toFixed(1) + 'M';
      }
      // billions
      else if(value >= 1000000000 && value <= 999999999999){
        return (value / 1000000000).toFixed(1) + 'B';
      }
      else
        return value;
    },

    // seconds to mm:ss format
    timeFormatter: function(secs) {
        secs = Math.round(secs);
        var hours = Math.floor(secs / (60 * 60));
        var divisor_for_minutes = secs % (60 * 60);
        var minutes = Math.floor(divisor_for_minutes / 60);
        if(minutes < 10){ minutes = "0" + minutes }
        var divisor_for_seconds = divisor_for_minutes % 60;
        var seconds = Math.ceil(divisor_for_seconds);
        if(seconds < 10){ seconds = "0" + seconds }
        if(hours > 0) {
            if(hours < 10){ hours = "0" + hours }
            return hours + ":" + minutes + ":" + seconds;
        }
        else
            return minutes + ":" + seconds;
    },

    // collect game stats for diplaying them in left bottom overlay
    // and send some to db
    

    resetGameVariables: function() {
        megaSlitherStats.deathReason = "";
    	userInterface.overlays.prefOverlay.style.display = 'none';
        megaSlitherStats.snakePosition = {x:0, y:0};
        snakPathLength = 0;
        megaSlitherStats.gameDuration = 0;
        megaSlitherStats.snakPathLength = 0;
        megaSlitherStats.killsInGame = 0;
        megaSlitherStats.gameId = 0;
        megaSlitherStats.snakeLegth = 0;
        localStorage.removeItem('megaslither_current_snake');
        megaSlitherStats.overlays.statsOverlay.style.bottom = "110px";
    },

    // write all events to localstorage
    storeObjectToLocalStorage: function(object) {
        var a = JSON.parse(localStorage.getItem('megaslither_events'));
        if(!a) {
            a = [];
        }
        a.push(object);
        localStorage.setItem('megaslither_events', JSON.stringify(a));
    },

    // get all events from localstorage for sending them to db with some interval 
    sendEventsFromLocalStorage: function() {
        var a = JSON.parse(localStorage.getItem('megaslither_events'));
        if(a && a.length) {
            var myEvent = a.shift();
            var invocation = new XMLHttpRequest();
            var url = 'http://megaslither.io/api/db/'+ myEvent.type;
            var callbackHandler = myEvent.callbackHandler;
            delete myEvent.type;
            delete myEvent.callbackHandler;
            window.sendData.connect(url, 'POST', myEvent, megaSlitherStats[callbackHandler]);
            // if(invocation) {
            //     invocation.open('POST', url, true);
            //     invocation.setRequestHeader('Content-Type', 'application/json');
            //     invocation.onreadystatechange = megaSlitherStats[callbackHandler];
            //     invocation.send(JSON.stringify(myEvent)); 
            // }
        }
        localStorage.setItem('megaslither_events', JSON.stringify(a));

        megaSlitherStats.savecurrentSnake();
    },

    savecurrentSnake: function() {
        if(window.lastscore && window.lastscore.childNodes[1] && window.lastscore.childNodes[1].innerHTML && window.playing) {
            localStorage.setItem('megaslither_current_snake', JSON.stringify({length: parseInt(window.lastscore.childNodes[1].innerHTML), time: megaSlitherStats.gameDuration,
                kills: megaSlitherStats.killsInGame, pathLength: megaSlitherStats.snakPathLength, serverIp: window.bso.ip}));            
        }

    },

    // API response handlers
    startGameCallbackHandler: function(e, response) {
        var game = JSON.parse(response);
        megaSlitherStats.gameId = game.id;
    },

    startSessionCallbackHandler: function(e, response) {
        var sessionId = JSON.parse(response);
        if(window.user) {
        	localStorage.setItem("megaslither_sessionId", sessionId.id);
        	window.user.session_id = sessionId.id;
        }

    },
    endGameCallbackHandler: function(e) {

    },
    endSessionCallbackHandler: function(e) {

    },
    leaderBoardCallbackHandler: function(e) {

    },
    killCallbackHandler: function(e) {

    },
    updateCallbackHandler: function(e) {

    },

    gameStatsCallbackHandler: function(e) {

    },

    // overrided ws.onmessage
    myOnMessage: function(b) {
        var originalB = b;
        b = new Uint8Array(b.data);
        if (2 <= b.length) {
            var c = b[0] << 8 | b[1];
            0 == lptm && (e = 0);
            var h = String.fromCharCode(b[2])
            var c = 3;

            // killed an oponnent
            if ("k" == h){
                megaSlitherStats.killsInGame = b[7];
				var distP = 1000;
				var killedSnake;
				for(i=0;i<snakes.length;i++){
					if(snakes[i].id == snake.id) continue;
					for(j=0;j<snake.pts.length;j++){
						var dist = Math.sqrt( (snakes[i].xx-(snake.pts[j].xx))*(snakes[i].xx-snake.pts[j].xx) 
						+ (snakes[i].yy-snake.pts[j].yy)*(snakes[i].yy-snake.pts[j].yy) )
						if(distP > dist) {
							distP = dist;
							killedSnake = snakes[i];
						}

					}
				}
				if(killedSnake.dead) {
	                megaSlitherStats.storeObjectToLocalStorage({callbackHandler: "killCallbackHandler", type:"insert/kill", "killer": {"id": megaSlitherStats.userId, 
	                     "length": Math.floor(15 * (window.fpsls[window.snake.sct] + window.snake.fam /
	                     window.fmlts[window.snake.sct] - 1) - 5) }, "victim": {"id": killedSnake.id, "length": Math.floor(15 * (fpsls[killedSnake.sct] + killedSnake.fam / fmlts[killedSnake.sct] - 1) - 5) / 1  }});
                }
            }
            // our snake has been killed
            if ("v" == h){
                megaSlitherStats.deathReason = "killed";
            	megaSlitherStats.snakeLegth = Math.floor(15 * (fpsls[snake.sct] + snake.fam / fmlts[snake.sct] - 1) - 5) / 1;
				// localStorage.lastscreenShot = mc.toDataURL("image/jpeg", 1);
				// big number to find nearest snake        
				var distP = 1000;
				var killerSnake;
				for(i=0;i<snakes.length;i++){
					if(snakes[i].id == snake.id) continue;
					for(j=0;j<snakes[i].pts.length;j++){
						var dist = Math.sqrt( (snakes[i].pts[j].xx-(snake.xx))*(snakes[i].pts[j].xx-snake.xx) 
						+ (snakes[i].pts[j].yy-snake.yy)*(snakes[i].pts[j].yy-snake.yy) )
						if(distP > dist) {
							distP = dist;
							killerSnake = snakes[i];
						}

					}
				}
                megaSlitherStats.storeObjectToLocalStorage({callbackHandler: "killCallbackHandler", type:"insert/kill", "killer": {"id": killerSnake.id, "length": Math.floor(15 * (fpsls[killerSnake.sct] + killerSnake.fam / fmlts[killerSnake.sct] - 1) - 5) / 1 }, 
                    "victim": {"id": window.user.id, "length": Math.floor(15 * (window.fpsls[window.snake.sct] + window.snake.fam /
                    window.fmlts[window.snake.sct] - 1) - 5)}});
            }
            if(ws && ws.original_onmessage)             
                ws.original_onmessage(originalB);
        }
    },
    // Update stats overlay.
    updateStats: function () {
        var oContent = [];
        var median, count = 10;

        // if (megaSlitherStats.userStats.length === 0) return;

        // median = Math.round((bot.scores[Math.floor((bot.scores.length - 1) / 2)] +
        //     bot.scores[Math.ceil((bot.scores.length - 1) / 2)]) / 2);
		if(snake) {
        	megaSlitherStats.snakeLegth = Math.floor(15 * (fpsls[snake.sct] + snake.fam / fmlts[snake.sct] - 1) - 5) / 1;
        }
        oContent.push('games: ' + megaSlitherStats.userStats.length);
        if(megaSlitherStats.userStats.length > 0) {
            oContent.push('rank: ' + window.rank + ' of ' + window.snake_count);
            oContent.push('kills cur\\max: ' + megaSlitherStats.killsInGame + "\\" + 
                (megaSlitherStats.userStats.reduce(function (a, b) { return {kills: a.kills > b.kills ? a.kills : b.kills}; }).kills )) ;
            oContent.push('time: ' + megaSlitherStats.timeFormatter(megaSlitherStats.gameDuration) + "\\" + 
                megaSlitherStats.timeFormatter(megaSlitherStats.userStats.reduce(function (a, b) { return {time: a.time > b.time ? a.time : b.time}; }).time ));
            oContent.push('path: ' + megaSlitherStats.lengthFormatter(megaSlitherStats.snakPathLength) + "\\" + 
                megaSlitherStats.lengthFormatter(megaSlitherStats.userStats.reduce(function (a, b) { return {pathLength: a.pathLength > b.pathLength ? a.pathLength : b.pathLength}; }).pathLength ));
            
            // oContent.push('at: ' + Math.round(
            //     megaSlitherStats.userStats.reduce(function (a, b) { return {length: a.length + b.length}; }).length / (megaSlitherStats.userStats.length)));
            if(snake)
                oContent.push("length:" + megaSlitherStats.snakeLegth  + "\\" + megaSlitherStats.userStats[0].length );
            else
                oContent.push("length: 0\\" + megaSlitherStats.userStats[0].length );
            // in case of first game
        } else {
            oContent.push('rank: ' + window.rank + ' of ' + window.snake_count);
            oContent.push('kills cur\\max: ' + megaSlitherStats.killsInGame + "\\" + megaSlitherStats.killsInGame) ;
            oContent.push('time: ' + megaSlitherStats.timeFormatter(megaSlitherStats.gameDuration) + "\\" + megaSlitherStats.timeFormatter(megaSlitherStats.gameDuration));
            oContent.push('path: ' + megaSlitherStats.lengthFormatter(megaSlitherStats.snakPathLength) + "\\" + megaSlitherStats.lengthFormatter(megaSlitherStats.snakPathLength));
            oContent.push("length: " + megaSlitherStats.snakeLegth  + "\\" + megaSlitherStats.snakeLegth );
        }

        megaSlitherStats.overlays.statsOverlay.innerHTML = oContent.join('<br/>');
    }
};

userInterface.collectStatistics = function() {

    if(window.user && window.user.id && !window.user.session_id && !megaSlitherStats.sessionRequestSent) {
        megaSlitherStats.storeObjectToLocalStorage({callbackHandler: "startSessionCallbackHandler", type:"start/session", "userId": window.user.id});
        megaSlitherStats.sessionRequestSent = true;
    }

    // Game statistics
    if (window.playing) {
        userInterface.overlays.prefOverlay.style.display = 'block';
        megaSlitherStats.overlays.statsOverlay.style.bottom = "5px";
        // remove game native score popup
        if(window.lbf) {
            window.lbf.left = "-100px";
            window.lbf.remove();
        }

        if(ws.onmessage && ws.original_onmessage == null)  {
            ws.original_onmessage = ws.onmessage;
            ws.onmessage = megaSlitherStats.myOnMessage;
        }
        if(!megaSlitherStats.startTime) {
            megaSlitherStats.startTime = Date.now();
            if(window.user && window.user.id)
                megaSlitherStats.storeObjectToLocalStorage({callbackHandler: "startGameCallbackHandler", type:"start/game", "userId": window.user.id, 
                    "sessionId": window.user.sessionId});
        } else {
            megaSlitherStats.gameDuration = parseInt((Date.now() - megaSlitherStats.startTime)/1000);
        }

        if(megaSlitherStats.snakePosition.x) {
            megaSlitherStats.snakPathLength += 
                parseInt( Math.sqrt( (megaSlitherStats.snakePosition.x-(window.snake.xx))*(megaSlitherStats.snakePosition.x-window.snake.xx) 
                + (megaSlitherStats.snakePosition.y-window.snake.yy)*(megaSlitherStats.snakePosition.y-window.snake.yy) ));
        }
        megaSlitherStats.snakePosition = {x: window.snake.xx, y: window.snake.yy};
     
        megaSlitherStats.updateStats();
    } else {
        //save and reset statistics after death
        if(megaSlitherStats.startTime){
            megaSlitherStats.gameDuration = parseInt((Date.now() - megaSlitherStats.startTime)/1000);
            megaSlitherStats.startTime = 0;
            if(window.lastscore && window.lastscore.childNodes[1] && window.lastscore.childNodes[1].innerHTML) {
                megaSlitherStats.userStats.push({length: parseInt(window.lastscore.childNodes[1].innerHTML), time: megaSlitherStats.gameDuration,
                                                kills: megaSlitherStats.killsInGame, pathLength: megaSlitherStats.snakPathLength});
                megaSlitherStats.userStats.sort(function (a, b) { return b.length - a.length; });

                megaSlitherStats.storeObjectToLocalStorage({callbackHandler: "gameStatsCallbackHandler", type:"game/stats", "userId": window.user.id,
                    "length": parseInt(window.lastscore.childNodes[1].innerHTML), "kills": megaSlitherStats.killsInGame, 
                    "path": megaSlitherStats.snakPathLength,"time": megaSlitherStats.gameDuration, "serverIp": window.bso.ip});
            }

            localStorage.setItem("megaslither_stats", JSON.stringify(megaSlitherStats.userStats));
            if(window.user) {
                megaSlitherStats.storeObjectToLocalStorage({callbackHandler: "updateCallbackHandler", type:"update/snake", "userId": window.user.id,
                    "sessionId": window.user.session_id, "gameId": megaSlitherStats.gameId, "length": megaSlitherStats.snakeLegth});
                megaSlitherStats.storeObjectToLocalStorage({callbackHandler: "endGameCallbackHandler", type:"end/game", "userId": window.user.id, 
                    "sessionId": window.user.sessionId, "gameId": megaSlitherStats.gameId});


            }


            megaSlitherStats.updateStats();
            megaSlitherStats.resetGameVariables();
        }
    }
};
setInterval(userInterface.collectStatistics, 200);
// send events to server
setInterval(megaSlitherStats.sendEventsFromLocalStorage, 5000);

// if browser crashes, or user closed window 
// get last game from localstorage and send stats to server
if(localStorage.getItem("megaslither_current_snake") && window.user.id) {
    stats = localStorage.getItem("megaslither_current_snake");
    megaSlitherStats.storeObjectToLocalStorage({callbackHandler: "gameStatsCallbackHandler", type:"game/stats", "userId": window.user.id,
        "length": stats.length, "kills": stats.kills, "path": stats.pathLength,"time": stats.time, "serverId": stats.serverId});
    localStorage.removeItem('megaslither_current_snake');
}


(function() {


	// init statistics overlay
	megaSlitherStats.initOverlay();

	//end previous session
	if(localStorage.getItem("megaslither_sessionId")) {
	    window.user.session_id = localStorage.getItem("megaslither_sessionId");
	    megaSlitherStats.storeObjectToLocalStorage({callbackHandler: "endSessionCallbackHandler", type:"end/session", "sessionId": window.user.session_id});
	    localStorage.removeItem("megaslither_sessionId");
	    localStorage.removeItem("megaslither_stats");
	}
	// if(localStorage.getItem("megaslither_stats")) {

	// 	megaSlitherStats.userStats = JSON.parse(localStorage.getItem("megaslither_stats"));
	// 	megaSlitherStats.updateStats();
	// }
	// init events item
	if(!localStorage.getItem('megaslither_events'))
		localStorage.setItem('megaslither_events',JSON.stringify([]))
})();