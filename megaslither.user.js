// ==UserScript==
// @name        MegaSlither.addon
// @namespace   http://megaslither.io
// @version     0.2.3
// @description MegaSlither.io MegaStat, MegaSkins, MegaBot
// @updateURL   https://bitbucket.org/icerbucket/megaslither.io/raw/develop/megaslither.user.js
// @author      Pirat New Time
// @icon        http://i.imgur.com/WH8UXOH.jpg
// @match       http://slither.io/
// @grant       none
// @supportURL  https://telegram.me/joinchat/APYUoT7RMJTnLxgHoBytgg
// ==/UserScript==


window.original_keydown = document.onkeydown;
window.user = {};
window.playh = document.getElementById('playh');
document.getElementById('tips').parentNode.removeChild(document.getElementById('tips'));
document.getElementById("nick_holder").parentNode.style = 'display: none';
document.getElementById("playh").style = 'display: none';

window.GM_info = {
	script: {
		version: '0.2.3.1'
	}
};


var s = document.createElement('script');
s.src = 'http://rawgit.com/j-c-m/Slither.io-bot/master/bot.user.js';
s.type = 'text/javascript';
s.onload = function() {

	userInterface.collectStatistics = function() {}
	/**
	 * Подменяем подсказки по командам
	 */
	userInterface.onPrefChange = function() {
	    // Set static display options here.
	    var oContent = [];
	    var ht = userInterface.handleTextColor;

	    oContent.push('[MegaSlither.io] v. ' + window.GM_info.script.version);
    	oContent.push('[O] qualtiy: <span style=\"color:' +
            (window.mobileRender ? 'red;\">low' : 'green;\">high') + '</span>' );
	    oContent.push('[T] bot: ' + ht(bot.isBotEnabled));


	    userInterface.overlays.prefOverlay.innerHTML = oContent.join('<br/>');
	};
    // Деактивируем ненужные функции
    userInterface.savePreference('logDebugging', false);
    userInterface.savePreference('visualDebugging', false);
    userInterface.savePreference('autoRespawn', false);
    userInterface.savePreference('mobileRender', false);

    // move pref Overlay to the screen top
    userInterface.overlays.prefOverlay.className = "";
    userInterface.overlays.prefOverlay.style.top = '4px';
    userInterface.overlays.prefOverlay.style.display = 'none';

    // move pref Overlay to the screen top
    userInterface.overlays.botOverlay.style.bottom = '60px';
    userInterface.overlays.botOverlay.style.textAlign = 'right';
    userInterface.overlays.botOverlay.style.width = '';
    userInterface.overlays.botOverlay.style.borderRadius = '';
    userInterface.overlays.botOverlay.style.padding = '';


    // replace stats overlay
    userInterface.overlays.statsOverlay.parentElement.removeChild(userInterface.overlays.statsOverlay)

	document.getElementById('nick').value = '';
	var scr = document.createElement('script');
	scr.src = 'https://bb.githack.com/icerbucket/megaslither.io/raw/develop/slither.addon.js';
	scr.type = 'text/javascript';
	
	scr.onload = function() {
		this.parentNode.removeChild(this);
		var scr2 = document.createElement('script');
		scr2.src = 'https://bb.githack.com/icerbucket/megaslither.io/raw/develop/megaslither.stats.js';
		scr2.type = 'text/javascript';
		scr2.onload = function() {
			this.parentNode.removeChild(this);
		};
		(document.body || document.head || document.documentElement).appendChild(scr2);
	};
	(document.body || document.head || document.documentElement).appendChild(scr);
	var scr1 = document.createElement('script');
	scr1.src = 'https://bb.githack.com/icerbucket/megaslither.io/raw/develop/auth.js';
	scr1.type = 'text/javascript';
	scr1.onload = function() {
		this.parentNode.removeChild(this);
	};
	(document.body || document.head || document.documentElement).appendChild(scr1);


};
(document.head || document.documentElement).appendChild(s);
