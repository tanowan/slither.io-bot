/**
 * Объект запроса к серверу
 */
window.sendData = {

    /**
     * Сервер по умолчанию
     */
    server: "",

    /**
     * Запрос к серверу
     *
     * @param {string} url Адрес запроса
     * @param {string} method Метод запроса (GET, POST..)
     * @param {string} data
     * @return {function} callback
     */
    connect: function(url, method, data, callback) {
        var xhr = (function() {
            if (typeof XMLHttpRequest === 'undefined') {
                XMLHttpRequest = function() {
                    try {
                        return new window.ActiveXObject("Microsoft.XMLHTTP");
                    } catch (e) {}
                };
            }
            return new XMLHttpRequest();
        })();
        xhr.open(method, url, true);
        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        //xhr.setRequestHeader('Content-length', data.length);
        //xhr.setRequestHeader('Connection', 'close');
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4 && xhr.status == 200) {
                callback(xhr.status, xhr.responseText);
            }
        };
        if (data) {
            xhr.send(data);
        } else {
            xhr.send();
        }
    }
};

/**
 * Чтение списка серверов из файла
 */
window.readServers = {
    reading: false, //статус прочтения
    stock: "", //чистые данные
    dstock: [], //расшифрованные данные
    /**
     * Чтение файла со списком сервером
     * или получение данных из localStorage
     */
    readfile: function() {
        window.readServers.reading = true;
        window.readServers.decodestock();
    },
    /**
     * Расшифровка списка серверов
     *
     * @param {string} stock Для теста других данных
     */
    decodestock: function() {
        window.readServers.dstock = window.sos;
        window.readServers.dstock.forEach(function(item, i) {
            window.readServers.dstock[i].id = i + 1;
            delete window.readServers.dstock[i].cluo;
        });
        localStorage.isServers = true;
        localStorage.servers = JSON.stringify(window.readServers.dstock);
    }
};

/**
 * Пинг игровых серверов
 * Формирование списка серверов
 */
window.pingServers = {
    countAll: 0, // всего серверов
    countNow: 0, // пропинговано
    readcountry: 0, // получено стран
    timeout: 5000, // время до следующей отправки пакета на пинг, мс
    sizepack: 5, // сколько серверов за раз пропинговать
    init_id: 0, // инициализация таймера (для функции stop())
    list: [], // строгий список серверов
    slist: [], // сортированный список
    /**
     * Инициализация всего процесса
     */
    init: function() {
        console.log('Init ping servers..');
        if (window.readServers.reading) {
            if (window.readServers.dstock == "") {
                setTimeout('window.pingServers.init()', 3000);
            } else {
                window.pingServers.list = window.readServers.dstock;
                window.pingServers.countAll = window.readServers.dstock.length;
                /*for(var key in window.bso.cluo.sos){
                    window.pingServers.list[key] = {id: this.countAll, ping: -1, ip: window.bso.cluo.sos[key].ip, po: window.bso.cluo.sos[key].po, ac: window.bso.cluo.sos[key].ac};
                    window.pingServers.countAll++;
                }*/
                window.pingServers.appendlist();
                window.pingServers.reqping();
                window.pingServers.getcountry();
                this.init_id = setInterval('window.pingServers.reqping()', this.timeout);
                //setInterval('window.pingServers.reflist()', 5000);
            }
        } else {
            window.readServers.readfile();
            setTimeout('window.pingServers.init()', 3000);
        }
    },
    /**
     * Остановить процесс
     */
    stop: function() {
        clearInterval(this.init_id);
    },
    /**
     * Функция отправки пакетов на пинг
     */
    reqping: function() {
        var ni = 0;
        while (this.list[this.countNow] && ni <= this.sizepack) {
            window.pingServers.ping(this.list[this.countNow].ip , window.pingServers.countNow);
            ni++;
            //window.pingServers.countNow = (window.pingServers.countNow+1)==window.pingServers.countAll ? 0 : window.pingServers.countNow+1;
            if (window.pingServers.countNow == window.pingServers.countAll) {
                window.pingServers.stop();
                break;
            } else {
                window.pingServers.countNow++;
            }
        }
        //window.pingServers.sorted();
    },
    /**
     * Пинг сервера в котором играем сейчас
     *
     * @param {string} ip:port
     * @param {int} i Номер объекта в массиве list
     */
    controlping: function(ip, i) {
        if (window.playing) {
            window.pingServers.ping(ip, i);
            setTimeout('window.pingServers.controlping("' + ip + '", ' + i + ')', this.timeout);
        }
    },
    /**
     * Пинг сервера - результат записывается в объект массива
     *
     * @param {string} ip:port
     * @param {int} clb Номер объекта в массиве list
     */
    ping: function(ip, clb) {
        var fws;
        try {
            var fws = new WebSocket('ws://' + ip + ':80/ptc');
        } catch (G) {
            fws = null;
        }

        if(fws) {
            fws.binaryType = "arraybuffer";
            var st = 0;
            fws.onopen = function(b) {
                b = !1;
                st = new Date().getTime();
                b = new Uint8Array(1);
                b[0] = 112;
                this.send(b);
             };
            fws.onmessage = function(e) {
                if (typeof clb !== 'undefined') {
                    var ping = (new Date().getTime()) - st;
                    window.pingServers.list[clb].ping = ping;
                    window.pingServers.list[clb].isping = true;
                    // Добавляем к пункту пинг
                    var item = window.pingServers.list[clb];
                    var option = document.getElementById('playh').querySelector('#server_' + item.id);
                    option.style.color = '#000';
                    if (ping >= 0 && ping < 51) {
                        option.style.backgroundColor = '#70F053';
                    } else if (ping > 50 && ping < 201) {
                        option.style.backgroundColor = '#E7EA22';
                    } else if (ping > 200) {
                        option.style.backgroundColor = '#ED1F1F';
                    }
                    option.innerHTML = item.countryCode + ' (#' + item.id + ') ' + item.ip + ':' + item.po + ' ' + ping + ' ms';
                } else {
                    console.log(ip + ' - ping ' + ((new Date().getTime()) - st));
                }
                if (window.bso === undefined || window.bso.ip === undefined || window.bso.ip + ':' + window.bso.po != ip) fws.close();
            };
            fws.onerror = function() {
                if (typeof clb !== 'undefined') {
                    window.pingServers.list[clb].ping = -1;

                    var item = window.pingServers.list[clb];
                    var option = document.getElementById('playh').querySelector('#server_' + item.id);
                    option.style.backgroundColor = '#000';
                    option.style.color = '#FFF';
                } else {
                    console.log(ip + ' error');
                }
            };
        }
    },
    /**
     * Добавляем в select список адреса
     */
    appendlist: function() {
        var select = document.getElementById('playh')
            .getElementsByClassName('taho')[0]
            .getElementsByClassName('sumsginp')[1];
        var i = 0;
        while (item = window.pingServers.list[i]) {
            var option = document.createElement('option');
            option.id = 'server_' + item.id;
            option.value = item.id;
            //option.value = item.ip+':'+item.po;
            option.style.backgroundColor = '#E1E1E1';
            option.style.color = '#000';
            option.title = item.ip + ':' + item.po;
            option.innerHTML = (item.countryCode || '') + ' (#' + item.id + ') ' + item.ip + ':' + item.po + ' ' + (item.isping ? item.ping : '??') + 'ms';

            select.appendChild(option);
            i++;
        }
    },
    /**
     * Сортировка списка по пингу
     */
    sorted: function() {
        window.pingServers.slist = window.pingServers.list;
        window.pingServers.slist.sort(function(i, ii) {
            if (i.ping > ii.ping) {
                return 1;
            } else if (i.ping < ii.ping) {
                return -1;
            } else {
                return 0;
            }
        });
    },
    /**
     * Создание внешнего окна со списком серверов
     */
    reflist: function() {
        var html = '<table style="width: 100%; color: wheat;">';
        var item;
        var i = 0;
        while (item = window.pingServers.slist[i]) {
            if (item.ping >= 0) html += '<tr><td style="text-align: right; padding-right: 10px;">' + (item.country ? item.countryCode : item.ip) + '(#' + item.id + ')' + '</td><td>' + item.ping + '</td></tr>';
            i++;
        }
        html += '</table>';
        userInterface.overlays.listServers.innerHTML = html;
    },
    /**
     * Получение списка стран для списка адресов
     */
    setcountry: 0,
    getcountry: function() {
        if (typeof localStorage.isCountry === 'undefined' || !localStorage.isCountry) {
            console.log('Read country... Now:' + window.pingServers.readcountry + ', All:' + window.pingServers.countAll);
            var post = [];
            for (var i = window.pingServers.readcountry; i < window.pingServers.readcountry + 100 && i < window.pingServers.countAll; i++) post.push({ query: window.pingServers.list[i].ip });
            window.sendData.connect('http://ip-api.com/batch?fields=country,countryCode,regionName', 'POST', JSON.stringify(post), function(status, data) {
                if (status != 200) return;
                data = JSON.parse(data);
                for (var o = 0; o < data.length; o++) {
                    var i = window.pingServers.setcountry;
                    window.pingServers.list[i].country = data[o].country;
                    window.pingServers.list[i].countryCode = data[o].countryCode;
                    window.pingServers.list[i].regionName = data[o].regionName;
                    var sid = window.pingServers.list[i].id;
                    var option = document.getElementById('playh').querySelector('#server_' + sid);
                    option.innerHTML = data[o].countryCode + ' (#' + sid + ') ' + window.pingServers.list[i].ip + ':' + window.pingServers.list[i].po + (window.pingServers.list[i].isping ? ' ' + window.pingServers.list[i].ping + ' ms' : ' ?? ms');
                    window.pingServers.setcountry++;
                }

            });
            window.pingServers.readcountry = i;
            if (i < window.pingServers.countAll) {
                setTimeout('window.pingServers.getcountry()', 5000);
            } else {
                console.log('All ip have country');
                // сделать сохранение в localStorage
                /*localStorage.servers = "";
                var jservers = "";
                var i = 0;
                var block = [];
                while(window.pingServers.list[i]){
                    block.push(window.pingServers.list[i]);
                    if(i==99 || i % 100 === 0){
                        if(i==99){
                            localStorage.servers = JSON.stringify(block);
                        }else{
                            localStorage.servers += (localStorage.servers).replace(']') + ',' + (JSON.stringify(block)).replace('[');
                        }
                        block = [];
                    }
                    i++;
                }
                i--;
                if(i>99 && i % 100 !== 0){
                    localStorage.servers += (localStorage.servers).replace(']') + ',' + (JSON.stringify(block)).replace('[');
                }
                console.log(window.pingServers.list);
                window.pingServers.stop();
                //localStorage.isCountry = true;
                //localStorage.servers = JSON.stringify(window.pingServers.list);*/
            }
        }
    }
};

/**
 * Подменяем элемент бота
 * Удаляем реакцию нажатия на некоторые клавиши
 */
userInterface.onkeydown = function(e) {
    // Original slither.io onkeydown function + whatever is under it
    //var original_keydown = document.onkeydown;
    window.original_keydown(e);
    if (window.playing) {
        // Letter `T` to toggle bot
        if (e.keyCode === 84) {
            bot.isBotEnabled = !bot.isBotEnabled;
            userInterface.savePreference('autoRespawn', !bot.isBotEnabled);
            window.autoRespawn = bot.isBotEnabled;
        }
        // Letter 'H' to toggle hidden mode
        if (e.keyCode === 72) {
            userInterface.toggleOverlays();
        }
        // Letter 'G' to toggle graphics
        if (e.keyCode === 71) {
            userInterface.toggleGfx();
        }
        // Letter 'O' to change rendermode (visual)
        if (e.keyCode === 79) {
            userInterface.toggleMobileRendering(!window.mobileRender);
        }
        // Letter 'Z' to reset zoom
        if (e.keyCode === 90) {
            canvas.resetZoom();
        }
        // Letter 'Q' to quit to main menu
        if (e.keyCode === 81) {
            window.autoRespawn = false;
            megaSlitherStats.deathReason = "quit";
            userInterface.quit();
        }
        // 'ESC' to quickly respawn
        if (e.keyCode === 27) {
            bot.quickRespawn();
        }
        userInterface.onPrefChange();
    }
};


/**
 * Подменяем блок информации о сервере и игре
 */
userInterface.onFrameUpdate = function() {
    // Botstatus overlay
    if (window.playing && window.snake !== null) {
        let oContent = [];
        var ping = window.force_ipid !== undefined && window.pingServers.list[window.force_ipid - 1].isping ? window.pingServers.list[window.force_ipid - 1].ping : '?';
        var color = "white";
        if(ping > -1 && ping <= 50) color = "green";
        if(ping > 50 && ping <= 200) color = "yellow";
        if(ping > 200) color = "red";
        oContent.push('Fps: ' + userInterface.framesPerSecond.fps + '<br/> Ping: <span style="color:' + color + ';"> ' + ping + ' ms</span>');

        userInterface.overlays.botOverlay.innerHTML = oContent.join('<br/>');

        if (userInterface.gfxOverlay) {
            let gContent = [];

            gContent.push('<b>' + window.snake.nk + '</b>');
            gContent.push(bot.snakeLength);
            gContent.push('[' + window.rank + '/' + window.snake_count + ']');

            userInterface.gfxOverlay.innerHTML = gContent.join('<br/>');
        }

        if (window.bso !== undefined && window.force_countryCode !== undefined &&
            userInterface.overlays.serverOverlay.innerHTML !==
            window.force_countryCode + ' #' + window.force_ipid + " " + window.bso.ip + ':' + window.bso.po) {

            userInterface.overlays.serverOverlay.innerHTML =
                window.force_countryCode + ' #' + window.force_ipid + " " + window.bso.ip + ':' + window.bso.po;

            window.pingServers.controlping(window.bso.ip , window.force_ipid - 1);

        }
    }
};

/**
 * Находим и устанавливаем глобальные переменные
 * window.force_countryCode & window.force_ipid
 * для текущего игрового процесса
 */
window.setCountry = {
    reading: false,
    /**
     * Найти и установить данные по стране
     *
     * @param {int} id Сообщаем строку из массива или происходит поиск
     */
    set: function(id) {
        if (window.playing && !window.setCountry.reading) {
            window.setCountry.reading = true;
            if (id) {
                var s = window.pingServers.list[id];
                if (s.countryCode === undefined) {
                    setTimeout('window.setCountry.set(' + id + ')', 1000);
                } else {
                    window.force_countryCode = s.countryCode;
                    window.force_ipid = s.id;
                }
                window.setCountry.reading = false;
            } else {
                for (var i = 0; i < window.pingServers.countAll; i++) {
                    if (window.pingServers.list[i].ip == window.bso.ip) {
                        console.log('OK', window.bso.ip, window.pingServers.list[i]);
                        if (window.pingServers.list[i].countryCode == undefined) break;
                        window.force_countryCode = window.pingServers.list[i].countryCode;
                        window.force_ipid = window.pingServers.list[i].id;
                        break;
                    }
                }
                if (window.force_countryCode === undefined) setTimeout('window.setCountry.set()', 1000);
                window.setCountry.reading = false;
            }

        } else {
            if (!window.setCountry.reading) setTimeout('window.setCountry.set(' + (id ? id : '') + ')', 1000);
        }
    }
};

/**
 * Соединение с выбранным сервером из select
 */
userInterface.playButtonClickListener = function() {
    userInterface.saveNick();
    userInterface.loadPreference('autoRespawn', false);
    userInterface.onPrefChange();

    var select = document.getElementById('playh').getElementsByTagName('select')[0];

    if (select.value) {
        console.log(window.pingServers.list, select.value)
        var s = window.pingServers.list[select.value - 1];
        console.log(s, s.countryCode);
        window.force_ip = s.ip;
        window.force_port = s.po;
        window.setCountry.set(select.value - 1);
        console.log('before connect');
        bot.connect();
    } else {
        window.force_ip = undefined;
        window.force_port = undefined;
        window.setCountry.set();
    }
};

(function() {
    bot.opt.radiusMult = 10;
    bot.isBotEnabled = !1;
    window.logDebugging = false;
    window.visualDebugging = false;
    window.autoRespawn = false;
    // Переназначаем интерфейс и элементы управления
    userInterface.onPrefChange();
    document.onkeydown = userInterface.onkeydown;
    window.play_btn.btnf.addEventListener('click', userInterface.playButtonClickListener);
    userInterface.overlays.serverOverlay.style.width = "";
    // Инициализация скрипта
    console.log('Start addon..');
    // Выбор сервера, ставим селект вместо инпута
    var lservers = document.getElementById('playh').getElementsByClassName('taho')[0];
    lservers.getElementsByClassName('sumsginp')[0].id = 'input_server';
    lservers.getElementsByClassName('sumsginp')[0].style.display = 'none';
    var selector = document.createElement('select');
    selector.className = 'sumsginp';
    selector.maxLength = 21;
    selector.style.width = '220px';
    selector.style.height = '24px';
    selector.setAttribute('onchange', "document.getElementById('playh').querySelector('#input_server').value = this.value;");
    var option = document.createElement('option');
    option.innerHTML = 'Auto';
    option.value = '';
    option.style.color = '#000';
    selector.appendChild(option);
    lservers.appendChild(selector);

    // Запуск пинга серверов
    window.pingServers.init();
})();
