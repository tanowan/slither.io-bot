var auth = window.auth = (function(window, document) {
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s);
		js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));

	window.statusChangeCallback = function(response) {
		if (response.status === 'connected') {
			testAPI(function(_response) {
				window.user = _response;
				if(megaSlitherStats && !megaSlitherStats.sessionRequestSent) {
		    		megaSlitherStats.storeObjectToLocalStorage({callbackHandler: "startSessionCallbackHandler", type:"start/session", "userId": window.user.id});
					megaSlitherStats.sessionRequestSent = true;
		    	}
				console.log(document.getElementById('nick').value);
				document.getElementById('nick').value = window.user.name;
				console.log(document.getElementById('nick').value);
				var xhr = new XMLHttpRequest();
				xhr.open('post', 'http://megaslither.io/api/db/auth', true);
				xhr.setRequestHeader('Content-Type', 'application/json');
				xhr.onreadystatechange = function() {
					console.log(xhr.responseText);
				};
				xhr.send(JSON.stringify(window.user));
			});
			removeLoginButton();
			insertLogoutButton();
		} else {
			if(megaSlitherStats) {
	    		megaSlitherStats.storeObjectToLocalStorage({callbackHandler: "endSessionCallbackHandler", type:"end/session", "sessionId": window.user.session_id});
	    		localStorage.removeItem("megaslither_sessionId");
				megaSlitherStats.sessionRequestSent = false;
	    	}
			insertLoginButton();
			removeLogoutButton();
		}
	}

	window.checkLoginState = function() {
		FB.getLoginStatus(function(response) {
			statusChangeCallback(response);
		});
	}

	window.fbAsyncInit = function() {
		FB.init({
			appId: '867585170013996',
			cookie: true,
			xfbml: true,
			version: 'v2.2'
		});

		FB.getLoginStatus(function(response) {
			statusChangeCallback(response);
		});

	};

	function testAPI(cb) {
		var cb = cb || function() {};
		console.log('Welcome!  Fetching your information.... ');
		FB.api('/me', function(response) {
			cb(response);
			console.log('Successful login for: ' + response.name);
		});
	}

	function createFBButton() {
		var fb = document.createElement('img');
		fb.id = 'fbLogin';
		fb.src = 'http://rhema.anatomy.org.za/anatomy5/pages/_imgs/facebook-login-button.png';
		fb.onclick = function() {
			FB.login(checkLoginState, { scope: 'public_profile' });
		};
		fb.style = 'margin-left: 39%; margin-top: 10px; width: 300px; height: 70px; cursor: pointer';
		return fb;
	}

	function insertLoginButton() {
		var before = document.getElementById('playh');
		if (!document.getElementById('fbLogin')) {
			var fb = createFBButton();
			before.parentNode.insertBefore(fb, before);
		}
		before.style = 'display: none';
		document.getElementById("nick_holder").parentNode.style = 'display: none';
	}

	function removeLoginButton() {
		var before = document.getElementById('playh');
		var fbLogin = document.getElementById('fbLogin');
		if (fbLogin) {
			fbLogin.parentNode.removeChild(fbLogin);
		}
		before.style = 'margin-left: auto; margin-right: auto; text-align: center; opacity: 1;';
		document.getElementById("nick_holder").parentNode.style = 'margin-left: auto; margin-right: auto; text-align: center;';
	}

	function insertLogoutButton() {
		var fb = document.createElement('div');
		fb.id = 'fbLogout';
		fb.innerHTML = "<fb:login-button autologoutlink='true' onlogin='checkLoginState'></fb:login-button>";
		document.getElementsByTagName('body')[0].appendChild(fb);
		FB.XFBML.parse(fb);
	}

	function removeLogoutButton() {
		var fb = document.getElementById('fbLogout');
		if (fb) {
			fb.parentNode.removeChild(document.getElementById('fbLogout'));
		}
	}

})(window, document);
